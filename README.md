
{{{{ Smart Thermostat }}}}

Description

IOT Thermostat using Tosmato ,MQTT and Node Red, the system will measure and displays the temperature and the humidity of the room. 
So, when the temperature is above the desired temperature a fan (LED) will switch off/on, a message and e-mail will send to the user .

Components

� NodeMCU esp8266
� 2 Buttons
� Breadboard
� Resisters
� Jumpers / wires
� Led